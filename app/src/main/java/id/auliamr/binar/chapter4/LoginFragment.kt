package id.auliamr.binar.chapter4

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.ContactsContract.DisplayNameSources.EMAIL
import android.provider.ContactsContract.Intents.Insert.EMAIL
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import id.auliamr.binar.chapter4.database.AppDatabase
import id.auliamr.binar.chapter4.databinding.FragmentHomeBinding
import id.auliamr.binar.chapter4.databinding.FragmentLoginBinding
import kotlinx.coroutines.*

class LoginFragment : Fragment() , View.OnClickListener {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!
    private var appDatabase : AppDatabase? = null
    private lateinit var sharedPreferences: SharedPreferences

    private lateinit var email: String
    private lateinit var password: String
    private var viewPass : Boolean = false
    private var cek: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sharedPreferences = requireActivity().getSharedPreferences(HomeFragment.PREF_USER, Context.MODE_PRIVATE)
        appDatabase = AppDatabase.getInstance(requireContext())

        binding.btnLogin.setOnClickListener(this)
        binding.tvBuatAkun.setOnClickListener(this)
        binding.btnViewPass.setOnClickListener(this)

        if (sharedPreferences.contains(HomeFragment.EMAIL)){
            Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_homeFragment)
        }
    }

    override fun onClick(view: View?) {
        when(view?.id){
            R.id.btn_login ->{
                login()
            }
            R.id.tv_buatAkun -> {
                view.findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
            }
            R.id.btn_view_pass -> {
                if (viewPass == false){
                    binding.apply {
                        btnViewPass.setImageResource(R.drawable.ic_remove_eye)
                        etPass.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    }
                    viewPass = true
                }else{
                    binding.apply {
                        btnViewPass.setImageResource(R.drawable.ic_stroke_eye)
                        etPass.transformationMethod = PasswordTransformationMethod.getInstance()
                    }
                    viewPass = false
                }
            }
        }
    }

    private fun login() {
        binding.apply {
            email = etEmail.text.toString()
            password = etPass.text.toString()
            cek = checkUser(email, view)
        }

        if (inputCheck(email,password,cek)){
            loginUser(email, password)
        }
    }

    private fun inputCheck(email: String, password: String, cek: Boolean) : Boolean{
        if (email.isEmpty() || password.isEmpty() || !cek
        ) {
            if (email.isEmpty()) {
                binding.apply {
                    etEmail.setError("Email harus diisi!")
                    etEmail.requestFocus()
                }

            }
            if (password.isEmpty()) {
                binding.apply {
                    etPass.setError("Password harus diisi!")
                    etPass.requestFocus()
                }
            }
            if (!cek) {
                binding.apply {
                    etEmail.setError("Format email tidak sesuai!")
                    etEmail.requestFocus()
                }
            }
            return false
        }else{
            return true
        }
    }

    private fun loginUser(email : String, password : String) {
        GlobalScope.async {
            val user = appDatabase?.UserDao()?.getUserRegistered(email)
            requireActivity().runOnUiThread{
                if (user != null) {
                    if (email == user.email && password == user.password){
                        val editor = sharedPreferences.edit()
                        editor.putString(HomeFragment.EMAIL, email)
                        editor.apply()
                        Navigation.findNavController(requireView()).navigate(R.id.action_loginFragment_to_homeFragment)
                    }else{
                        Toast.makeText(requireContext(), "Password yang anda masukkan salah", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(requireContext(), "Akun dengan email ${email} belum terdaftar", Toast.LENGTH_SHORT).show()
                }
            }

        }
    }

    private fun checkUser(email: String, view: View?): Boolean {
        CoroutineScope(Dispatchers.IO). launch {
            var result = appDatabase?.UserDao()?.getUserRegistered(email)
            if (result != null ) {
                    CoroutineScope(Dispatchers.Main).launch {
                        if (view != null) {
                            view.findNavController()
                                .navigate(R.id.action_loginFragment_to_homeFragment)
                        }
                    }
                }
             else {
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Email atau Password yang dimasukkan tidak sesuai.", Toast.LENGTH_SHORT).show()
                }
            }
        }
        return true
    }

}