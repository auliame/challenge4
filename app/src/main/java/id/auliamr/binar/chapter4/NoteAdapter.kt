package id.auliamr.binar.chapter4

import android.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import id.auliamr.binar.chapter4.database.AppDatabase
import id.auliamr.binar.chapter4.database.Note
import id.auliamr.binar.chapter4.databinding.DialogAddEditBinding
import id.auliamr.binar.chapter4.databinding.DialogDeleteBinding
import id.auliamr.binar.chapter4.databinding.ItemDataBinding
import kotlinx.android.synthetic.main.dialog_add_edit.view.*
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

@DelicateCoroutinesApi
class NoteAdapter(private val listNote: List<Note>, val context: HomeFragment) : RecyclerView.Adapter<NoteAdapter.ViewHolder>() {
        class ViewHolder(var binding: ItemDataBinding) : RecyclerView.ViewHolder(binding.root)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val binding = ItemDataBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ViewHolder(binding)
        }

        override fun getItemCount(): Int {
            return listNote.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val note = listNote[itemCount-1-position]
            holder.binding.apply {

                tvJudul.text = note.judul
                tvIsi.text = note.catatan

                btnIcDelete.setOnClickListener {
                    val bindingDelete = DialogDeleteBinding.inflate(LayoutInflater.from(it.context))
                    val dialogBuilder = AlertDialog.Builder(it.context)
                    dialogBuilder.setView(bindingDelete.root)

                    val dialog = dialogBuilder.create()
                    dialog.show()

                    bindingDelete.apply {
                        btnCancel.setOnClickListener {
                            dialog.dismiss()
                        }
                        btnIcDelete.setOnClickListener {
                            deleteData(note, holder, bindingDelete.root)
                            dialog.dismiss()
                        }
                    }
                }

                btnIcEdit.setOnClickListener {
                    val bindingEdit = DialogAddEditBinding.inflate(LayoutInflater.from(it.context))
                    val dialogBuilder = AlertDialog.Builder(it.context)
                    dialogBuilder.setView(bindingEdit.root)

                    val dialog = dialogBuilder.create()
                    val txtKeterangan = "Edit Data"
                    val txtButton = "EDIT"

                    bindingEdit.apply {
                        tvKeterangan.text = txtKeterangan
                        inputTitle.setText(note.judul)
                        inputNote.setText(note.catatan)
                        btnSave.setOnClickListener {

                            val judul = bindingEdit.inputTitle.text.toString()
                            val catatan = bindingEdit.inputNote.text.toString()

                            if (checkInput(judul, catatan, bindingEdit.root)){
                                val noteUpdated = Note(note.id, judul, catatan, note.email)
                                editNote(holder, noteUpdated, bindingEdit.root, dialog)
                            }
                        }
                    }

                    dialog.show()
                }
            }

        }


        private fun checkInput(judul: String, catatan: String, view: View) : Boolean{
            if (judul.isEmpty() || catatan.isEmpty()) {
                if (catatan.isEmpty()) {
                    view.apply {
                        input_note.setError("Catatan tidak boleh kosong")
                        input_note.requestFocus()
                    }
                }
                if (judul.isEmpty()) {
                    view.apply {
                        input_title.setError("Judul tidak boleh kosong")
                        input_title.requestFocus()
                    }
                }
                return false
            }else{
                return true
            }
        }


        private fun editNote(
            holder: ViewHolder,
            noteUpdated: Note,
            view: View,
            dialog: AlertDialog
        ) {
            val noteDatabase = AppDatabase.getInstance(holder.itemView.context)

            GlobalScope.async {
                val result = noteDatabase?.NoteDao()?.updateNote(noteUpdated)
                context.requireActivity().runOnUiThread {
                    if (result != 0) {
                        Toast.makeText(
                            view.context,
                            "Note berhasil diupdate",
                            Toast.LENGTH_LONG
                        ).show()
                    } else {
                        Toast.makeText(
                            view.context,
                            "Note Gagal diupdate",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
                dialog.dismiss()
                context.fetchData()
            }
        }

        private fun deleteData(note: Note, holder: ViewHolder, view: View) {
            val noteDatabase = AppDatabase.getInstance(holder.itemView.context)

            GlobalScope.async {
                val result = noteDatabase?.NoteDao()?.deleteNote(note)

                context.requireActivity().runOnUiThread {
                    if (result != 0) {
                        Toast.makeText(
                            view.context,
                            "Note berhasil dihapus",
                            Toast.LENGTH_LONG
                        ).show()
                    } else {
                        Toast.makeText(
                            view.context,
                            "Note Gagal dihapus",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }

                context.fetchData()
            }

        }

}