package id.auliamr.binar.chapter4

import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import id.auliamr.binar.chapter4.database.AppDatabase
import id.auliamr.binar.chapter4.database.User
import id.auliamr.binar.chapter4.databinding.FragmentRegisterBinding
import kotlinx.coroutines.*
import java.util.regex.Pattern

class RegisterFragment : Fragment() , View.OnClickListener {
    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private var appDatabase : AppDatabase? = null

    private lateinit var name: String
    private lateinit var email: String
    private lateinit var password: String
    private var cek: Boolean = false
    private var viewPass : Boolean = false
    private var viewKonfPass : Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appDatabase = AppDatabase.getInstance(requireContext())
        binding.apply {
            btnRegister.setOnClickListener(this@RegisterFragment)
            btnIcViewPass.setOnClickListener(this@RegisterFragment)
            btnIcViewKonfPass.setOnClickListener(this@RegisterFragment)
        }
    }

    override fun onClick(p0: View?) {
        when(p0?.id){
            R.id.btn_register -> {
                register()
            }
            R.id.btn_ic_view_pass ->{
                if (viewPass == false){
                    binding.apply {
                        btnIcViewPass.setImageResource(R.drawable.ic_remove_eye)
                        inputBuatPass.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    }
                    viewPass = true
                }else{
                    binding.apply {
                        btnIcViewPass.setImageResource(R.drawable.ic_stroke_eye)
                        inputKonfPass.transformationMethod = PasswordTransformationMethod.getInstance()
                    }
                    viewPass = false
                }
            }
            R.id.btn_ic_view_konfPass -> {
                if (viewKonfPass == false){
                    binding.apply {
                        btnIcViewKonfPass.setImageResource(R.drawable.ic_stroke_eye)
                        inputKonfPass.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    }
                    viewKonfPass = true
                }else{
                    binding.apply {
                        btnIcViewKonfPass.setImageResource(R.drawable.ic_remove_eye)
                        inputKonfPass.transformationMethod = PasswordTransformationMethod.getInstance()
                    }
                    viewKonfPass = false
                }
            }
        }
    }

    private fun register(){
        binding.apply {
            name = inputUname.text.toString()
            email = inputEmail.text.toString()
            password = inputBuatPass.text.toString()
            cek = checkUser(email, view)
        }

        if (inputCheck(name, email, password, cek)){
            registerUser(name, email, password)
        }
    }

    private fun registerUser(name: String, email: String, password: String) {
        val user = User(email, name, password)
        GlobalScope.async {
            val cekUser = appDatabase?.UserDao()?.getUserRegistered(email)
            if (cekUser != null) {
                requireActivity().runOnUiThread {
                    Toast.makeText(
                        requireContext(),
                        "User dengan email ${user.email} sudah terdaftar",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                val result = appDatabase?.UserDao()?.registerUser(user)
                requireActivity().runOnUiThread {
                    if (result != 0.toLong()) {
                        Toast.makeText(
                            requireContext(),
                            "Sukses mendaftarkan ${user.email}, silakan mencoba untuk login",
                            Toast.LENGTH_LONG
                        ).show()
                        Navigation.findNavController(requireView())
                            .navigate(R.id.action_registerFragment_to_loginFragment)
                    } else {
                        Toast.makeText(
                            requireContext(),
                            "Gagal mendaftarkan ${user.email}, silakan coba lagi",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            }

        }
    }


    private fun inputCheck(name: String, email: String, password: String, cek: Boolean) : Boolean {
        if (name.isEmpty() || email.isEmpty() || password.isEmpty() || !cek
            || binding.inputKonfPass.text.toString() != password || password.length < 6
        ) {
            if (name.isEmpty()) {
                binding.apply {
                    inputUname.setError("Username Tidak Boleh Kosong")
                    inputUname.requestFocus()
                }

            }
            if (email.isEmpty()) {
                binding.apply {
                    inputEmail.setError("Email Tidak Boleh Kosong")
                    inputEmail.requestFocus()
                }
            }
            if (password.isEmpty()) {
                binding.apply {
                    inputBuatPass.setError("Password Tidak Boleh Kosong")
                    inputBuatPass.requestFocus()
                }
            }
            if (!cek) {
                binding.apply {
                    inputEmail.setError("Email Tidak Sesuai Format")
                    inputEmail.requestFocus()
                }
            }
            if (binding.inputKonfPass.text.toString() != password) {
                binding.apply {
                    inputKonfPass.setError("Password Tidak Sama")
                    inputKonfPass.requestFocus()
                }

            }
            if (password.length < 6) {
                binding.apply {
                    inputBuatPass.setError("Password minimal 6 karakter")
                    inputBuatPass.requestFocus()
                }
            }
            return false
        }else{
            return true
        }
    }

    private fun checkUser(email: String, view: View?): Boolean {
        CoroutineScope(Dispatchers.IO). launch {
            var result = appDatabase?.UserDao()?.getUserRegistered(email)
            if (result != null){
                CoroutineScope(Dispatchers.Main).launch {
                    if (view != null) {
                        view.findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
                    }
                }
            } else {
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Email atau Password yang dimasukkan tidak sesuai.", Toast.LENGTH_SHORT).show()
                }
            }
        }

        return true

    }

}

